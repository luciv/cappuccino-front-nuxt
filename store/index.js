import jsonData from '../json/items.json';

export const state = () => ({
    jsonData,
    choice: "",
    templatesCloseStatus: false,
    templatesChoice: [],
    structure: {
        nav: ""
    }
});

export const mutations = {
    changeChoice(state, choice) {
        state.choice = choice;
    },
    changeTemplatesCloseStatus(state, templatesCloseStatus) {
        state.templatesCloseStatus = templatesCloseStatus;
        state.choice = "";
    },
    addTemplatesChoice(state, templatesChoice) {
        state.templatesChoice.push(templatesChoice);
    },
    changeStructureElement(state, structureElement) {
        const { type, value } = structureElement
        state.structure[type] = value;
    }
}

export const getters = {
    templates: (state) => {
        return state.jsonData.items;
    }
}